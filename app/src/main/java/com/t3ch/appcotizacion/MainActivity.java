package com.t3ch.appcotizacion;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    //Declaro 2 variables para mi XML inicial
    private EditText txtCliente;
    private Button btnCotizacion;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Relaciono mis variables con mi XML
        txtCliente = (EditText) findViewById(R.id.txtCliente);
        btnCotizacion = (Button) findViewById(R.id.btnCotizar);

        //Atento al primer click sobre mi boton
        btnCotizacion.setOnClickListener(new View.OnClickListener()
        {
            //En caso de click
            @Override
            public void onClick(View v)
            {
                if(txtCliente.getText().toString().trim().equalsIgnoreCase("")) //Si esta vacio
                {   //mando mensaje
                    Toast.makeText(MainActivity.this,"Favor de introducir un nombre para continuar",Toast.LENGTH_SHORT).show();
                }
                else // Si no
                {   //Llamo a la siguiente actividad y le paso el dato "cliente" con el putExtra
                    Intent icotiza = new Intent(MainActivity.this,CotizacionActividad.class);
                    icotiza.putExtra("cliente",txtCliente.getText().toString());
                    startActivity(icotiza); //Inicia la otra actividad
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.layout.activity_main)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}